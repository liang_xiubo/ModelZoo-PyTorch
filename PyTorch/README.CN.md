# 欢迎使用Ascend ModelZoo

为方便更多开发者使用Ascend ModelZoo，我们将持续增加典型网络和相关预训练模型。下面目录中我们罗列出一些重点模型。如果您有任何需求，请在[modelzoo/issues](https://gitee.com/ascend/modelzoo/issues)提交issue，我们会及时处理。

## 如何贡献

在开始贡献之前，请先阅读[CONTRIBUTING](https://gitee.com/ascend/modelzoo/blob/master/CONTRIBUTING.md)。
谢谢！

## 目录


### PyTorch

#### built-in

- [3D_ResNet_ID0421_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/cv/classification/3D_ResNet_ID0421_for_PyTorch)
- [Arcface_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/cv/classification/Arcface_for_PyTorch)
- [CRNN_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/cv/classification/CRNN_for_PyTorch)
- [DINO_ID3436_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/cv/classification/DINO_ID3436_for_PyTorch)
- [DeepMar_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/cv/classification/DeepMar_for_PyTorch)
- [Densenet121_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/cv/classification/Densenet121_for_PyTorch)
- [EfficientNet_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/cv/classification/EfficientNet_for_PyTorch)
- [MobileNetV2_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/cv/classification/MobileNetV2_for_PyTorch)
- [ResNet1001_1202_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/cv/classification/ResNet1001_1202_for_PyTorch)
- [ResNet50_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/cv/classification/ResNet50_for_PyTorch)
- [Shufflenetv2_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/cv/classification/Shufflenetv2_for_PyTorch)
- [SimMIM_ID3449_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/cv/classification/SimMIM_ID3449_for_PyTorch)
- [Transformer-SSL_ID3434_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/cv/classification/Transformer-SSL_ID3434_for_PyTorch)
- [DB_ID0706_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/cv/detection/DB_ID0706_for_PyTorch)
- [DAL_ID2732_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/cv/detection/DAL_ID2732_for_PyTorch)
- [Faster_Mask_RCNN_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/cv/detection/Faster_Mask_RCNN_for_PyTorch)
- [PSENet_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/cv/detection/PSENet_for_PyTorch)
- [RFCN_ID0418_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/cv/detection/RFCN_ID0418_for_PyTorch)
- [RetinaNet_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/cv/detection/RetinaNet_for_PyTorch)
- [SCRFD_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/cv/detection/SCRFD_for_PyTorch)
- [YOLOX_Dynamic_ID4069_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/cv/detection/YOLOX_Dynamic_ID4069_for_PyTorch)
- [SSD_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/cv/detection/SSD_for_PyTorch)
- [YOLOV4_ID0396_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/cv/detection/YOLOV4_ID0396_for_PyTorch)
- [YoloV3_ID1790_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/cv/detection/YoloV3_ID1790_for_PyTorch)
- [GOTURN_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/cv/object_tracking/GOTURN_for_PyTorch)
- [Bert-Squad_ID0470_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/nlp/Bert-Squad_ID0470_for_PyTorch)
- [Bert_Chinese_ID3433_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/nlp/Bert_Chinese_ID3433_for_PyTorch)
- [BiLSTM-CRF_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/nlp/BiLSTM-CRF_for_PyTorch)
- [GRU_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/nlp/GRU_for_PyTorch)
- [LSTM_ID0468_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/nlp/LSTM_ID0468_for_PyTorch)
- [mBART_ID2372_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/nlp/mBART_ID2372_for_PyTorch)
- [Transformer_ID0105_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/nlp/Transformer_ID0105_for_PyTorch)
- [XLM_ID0740_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/nlp/XLM_ID0740_for_PyTorch)
- [CPM_Finetune_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/nlp/CPM_Finetune_for_PyTorch)
- [DeCLIP_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/others/DeCLIP_for_PyTorch)
- [ESPnet_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/audio/ESPnet_for_PyTorch)
- [ESPnet_Dynamic_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/audio/ESPnet_Dynamic_for_PyTorch)
- [DeepLabv3+_ID1695_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/cv/semantic_segmentation/DeepLabv3+_ID1695_for_PyTorch)
- [DynamicUNet_for_Pytorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/built-in/cv/semantic_segmentation/DynamicUNet_for_Pytorch)
    
####  contrib
    
- [ResNet18_ID1593_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/contrib/cv/classification/ResNet18_ID1593_for_PyTorch)
- [MAE_for_PyTorch](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/contrib/cv/classification/MAE_for_PyTorch)
- [Swin-Transformer](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/contrib/cv/classification/Swin-Transformer)
- [Moco-v2](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/contrib/cv/classification/Moco-v2)
- [Cascade_RCNN](https://gitee.com/ascend/ModelZoo-PyTorch/tree/master/PyTorch/contrib/cv/detection/Cascade_RCNN)
    




					
  
		
## 免责声明

Ascend ModelZoo仅提供公共数据集下载和预处理脚本。这些数据集不属于ModelZoo，ModelZoo也不对其质量或维护负责。请确保您具有这些数据集的使用许可。基于这些数据集训练的模型仅可用于非商业研究和教育。

致数据集所有者：

如果您不希望您的数据集公布在ModelZoo上或希望更新ModelZoo中属于您的数据集，请在Github/Gitee提交issue，我们将根据您的issue删除或更新您的数据集。衷心感谢您对我们社区的理解和贡献。

Ascend ModelZoo的license是Apache 2.0.具体内容，请参见LICENSE文件。

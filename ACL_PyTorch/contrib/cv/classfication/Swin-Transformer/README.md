# Swin-Transformer模型-推理指导


- [概述](#ZH-CN_TOPIC_0000001172161501)

- [推理环境准备](#ZH-CN_TOPIC_0000001126281702)

- [快速上手](#ZH-CN_TOPIC_0000001126281700)

  - [获取源码](#section4622531142816)
  - [准备数据集](#section183221994411)
  - [模型推理](#section741711594517)

- [模型推理性能](#ZH-CN_TOPIC_0000001172201573)

- [配套环境](#ZH-CN_TOPIC_0000001126121892)

  ******





# 概述<a name="ZH-CN_TOPIC_0000001172161501"></a>

Swin-Transformer是针对于图片处理设计的基于Transformer架构的神经网络。该网络针对原始Transformer迁移到图片端后计算量过大，复用困难的问题，提出了新的swin-block以代替原有的attention架构。模型以窗口的attention方式极大地减少了图像不同区域间的互相响应，同时也避免了部分冗余信息的产生。最终，模型在减少了大量计算量的同时，在不同的视觉传统任务上也有了效果的提升。


- 参考实现：

  ```
  url=https://github.com/microsoft/Swin-Transformer
  branch=master
  commit_id=014eb33148a5e41576dd91715d5c557896613f51
  ```
  通过Git获取对应commit\_id的代码方法如下：

  ```
  git clone {repository_url}        # 克隆仓库的代码
  cd {repository_name}              # 切换到模型的代码仓目录
  git checkout {branch/tag}         # 切换到对应分支
  git reset --hard {commit_id}      # 代码设置到对应的commit_id（可选）
  cd {code_path}                    # 切换到模型代码所在路径，若仓库下只有该模型，则无需切换
  ```


## 输入输出数据<a name="section540883920406"></a>

- 输入数据

  | 输入数据 | 数据类型 | 大小                      | 数据排布格式 |
  | -------- | -------- | ------------------------- | ------------ |
  | image    | RGB_FP32 | batchsize x 3 x 224 x 224 | NCHW         |


- 输出数据

  | 输出数据 | 大小     | 数据类型 | 数据排布格式 |
  | -------- | -------- | -------- | ------------ |
  | labels  | 1 x 1000 | FLOAT32  | ND           |


# 推理环境准备\[所有版本\]<a name="ZH-CN_TOPIC_0000001126281702"></a>

- 该模型需要以下插件与驱动

  **表 1**  版本配套表

| 配套                                                         | 版本    | 环境准备指导                                                 |
| ------------------------------------------------------------ | ------- | ------------------------------------------------------------ |
| 固件与驱动                                                   | 1.0.15  | [Pytorch框架推理环境准备](https://www.hiascend.com/document/detail/zh/ModelZoo/pytorchframework/pies) |
| CANN                                                         | 5.1.RC2 | -                                                            |
| Python                                                       | 3.7.5   | -                                                            |
| PyTorch                                                      | 1.8.0   | -                                                            |
| 说明：Atlas 300I Duo 推理卡请以CANN版本选择实际固件与驱动版本。 | \       | \                                                            |

# 快速上手<a name="ZH-CN_TOPIC_0000001126281700"></a>

## 获取源码<a name="section4622531142816"></a>
1. 安装依赖。

   ```
   pip3 install -r requirements.txt
   ```
2. 获取开源代码仓。
   在已下载的源码包根目录下，执行如下命令。
   ```
   git clone https://github.com/microsoft/Swin-Transformer
   cd Swin-Transformer
   git checkout 6bbd83ca617db8480b2fb9b335c476ffaf5afb1a
   patch -p1 < ../swin.patch
   cd .. 
   ```
## 准备数据集<a name="section183221994411"></a>
1. 获取原始数据集。（解压命令参考tar –xvf  \*.tar与 unzip \*.zip）

   本模型支持ImageNet 50000张图片的验证集。以ILSVRC2012为例，请用户需自行获取ILSVRC2012数据集，上传数据集到服务器任意目录并解压（如：/home/HwHiAiUser/dataset）。本模型将使用到预处理后的target.json文件作为最后的比对标签。target.json文件需要放置在代码目录下。

   数据目录结构请参考：
   ```
   ├──ImageNet
    ├──ILSVRC2012_img_val
   ```

2. 数据预处理。\(请拆分sh脚本，将命令分开填写\)

   数据预处理将原始数据集转换为模型输入的数据。

   执行“swin_preprocess.py”脚本，完成预处理。

   ```
   mkdir ${prep_output_dir}
   python3.7 swin_preprocess.py --cfg=Swin-Transformer/configs/swin_tiny_patch4_window7_224.yaml --data-path=${datasets_path}/imagenet --bin_path=${prep_output_dir}
   ```
   --cfg：模型配置文件。

   --data-path：原始数据验证集（.jpeg）所在路径。

   --bin_path：输出的二进制文件（.bin）所在路径。

   每个图像对应生成一个二进制文件。运行成功后，在当前目录下生成二进制文件夹。

## 模型推理<a name="section741711594517"></a>

1. 模型转换。

   使用PyTorch将模型权重文件.pth转换为.onnx文件，再使用ATC工具将.onnx文件转为离线推理模型文件.om文件。

   1. 获取权重文件。

       获取权重文件“swin_tiny_patch4_window7_224.pth”，放到“./resume”目录下。[[权重文件下载链接](https://github.com/SwinTransformer/storage/releases/download/v1.0.0/swin_tiny_patch4_window7_224.pth)]

      ```
      mkdir resume
      mv swin_tiny_patch4_window7_224.pth ./resume/
      ```

   2. 导出onnx文件。

      使用“swin_pth2onnx.py”导出onnx文件。

      运行“swin_pth2onnx.py”脚本。

      ```
      mkdir onnx_models 
      python3.7 swin_pth2onnx.py --resume=resume/swin_tiny_patch4_window7_224.pth --cfg=Swin-Transformer/configs/swin_tiny_patch4_window7_224.yaml --batch-size=16
      ```

      获得“swin_b16.onnx”文件。

   3. 使用ATC工具将ONNX模型转OM模型。

      1. 配置环境变量。

         ```
          source /usr/local/Ascend/ascend-toolkit/set_env.sh
         ```

         > **说明：** 
         >该脚本中环境变量仅供参考，请以实际安装环境配置环境变量。详细介绍请参见《[CANN 开发辅助工具指南 \(推理\)](https://support.huawei.com/enterprise/zh/ascend-computing/cann-pid-251168373?category=developer-documents&subcategory=auxiliary-development-tools)》。

      2. 执行命令查看芯片名称（$\{chip\_name\}）。

         ```
         npu-smi info
         #该设备芯片名为Ascend310P3 （自行替换）
         回显如下：
         +-------------------+-----------------+------------------------------------------------------+
         | NPU     Name      | Health          | Power(W)     Temp(C)           Hugepages-Usage(page) |
         | Chip    Device    | Bus-Id          | AICore(%)    Memory-Usage(MB)                        |
         +===================+=================+======================================================+
         | 0       310P3     | OK              | 15.8         42                0    / 0              |
         | 0       0         | 0000:82:00.0    | 0            1074 / 21534                            |
         +===================+=================+======================================================+
         | 1       310P3     | OK              | 15.4         43                0    / 0              |
         | 0       1         | 0000:89:00.0    | 0            1070 / 21534                            |
         +===================+=================+======================================================+
         ```

      3. 执行ATC命令。
         ```
         atc --framework=5 --model=onnx_models/swin_b16.onnx  --output=swin_b16 --input_format=NCHW --input_shape="image:16,3,224,224" --log=debug --soc_version=${chip_name}
         ```

         - 参数说明：

           -   --model：为ONNX模型文件。
           -   --framework：5代表ONNX模型。
           -   --output：输出的OM模型。
           -   --input\_format：输入数据的格式。
           -   --input\_shape：输入数据的shape。
           -   --log：日志级别。
           -   --soc\_version：处理器型号。

           运行成功后生成swin_b16.om模型文件。



2. 开始推理验证。

   a.  使用ais-infer工具进行推理。

      ```
      chmod u+x ./tools/ais-bench_workload/tool/ais_infer/ais_infer.py
      ```

      ais-infer工具获取及使用方式请点击查看[[ais_infer 推理工具使用文档](https://gitee.com/ascend/tools/tree/master/ais-bench_workload/tool/ais_infer)]


   b.  执行推理。

      ```
      python ./tools/ais-bench_workload/tool/ais_infer/ais_infer.py --model onnx_models/swin_b16.om --input ${prep_output_dir --output ./result_bs16/ --outfmt TXT --batchsize 16  
      ```

      -   参数说明：

           -   --model：om文件路径。
           -   --input：预处理完的数据集文件夹
           -   --output：推理结果保存地址
           -   --outfmt：推理结果保存格式
           -   --batchsize：batchsize大小

      推理后的输出在--output指定目录下。

      >**说明：** 
      >执行ais-infer工具请选择与运行环境架构相同的命令。参数详情请参见[[ais_infer 推理工具使用文档](https://gitee.com/ascend/tools/tree/master/ais-bench_workload/tool/ais_infer)]
。

   c.  精度验证。

      调用swin_postprocess.py脚本与数据集标签target.json比对，可以获得Accuracy数据，结果保存在result.json中。

      注：需把ais-infer工具推理完成后推理结果保存地址中的sumary.json文件删除，否则运行精度验证脚本会报错

      ```
      python3.7 swin_postprocess.py --result_path=result_bs16/2022_09_01-18_51_23/ --target_file=target.json --save_path=./result_bs16.json
      ```
      注：--result_path指定的result_bs16/2022_09_01-18_51_23/路径不是固定，具体路径为ais-infer工具推理命令中，--output指定目录下的生成推理结果所在路径

      --result_path：生成推理结果所在路径
    
      --target_file：标签数据
    
      --save_path：生成结果文件


   d.  性能验证。

      可使用ais_infer推理工具的纯推理模式验证不同batch_size的om模型的性能，参考命令如下：

      ```
      python3.7 ${ais_infer_path}/ais_infer.py --model=${om_model_path} --loop=20 --batchsize=${batch_size}
      ```


# 模型推理性能&精度<a name="ZH-CN_TOPIC_0000001172201573"></a>

性能参考下列数据。

| batchsize | 1       | 4       | 8       | 16      | 32      | 64      |
|-----------|---------|---------|---------|---------|---------|---------|
| 310       | 201.9fps | 253.3fps  | 243.4fps | 237.6fps | 238.8fps | 243.2fps |
| 310P      | 244.0fps | 298.4fps | 371.0fps | 342.4fps | 288.5fps | 300.4fps |
| T4        | 267.7fps  | 384.7fps  | 387.6fps  | 270.8fps | 232.6fps | 188.97fps  |

精度参考下列数据。
|      | top1     | top5|
|------|----------|-------|
| 310       | 81.19%   | 95.5%|
| 310P      | 81.17%   | 95.5%|